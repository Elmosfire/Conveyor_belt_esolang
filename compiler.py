import networkx as nx
from networkx.drawing.nx_pydot import to_pydot
from collections import deque,namedtuple
import sys
from subprocess import Popen


Package = namedtuple('Package','x,y,dir')
DIRS = [(-1,0),(0,-1),(1,0),(0,1)]
formirror  = [3,2,1,0]
backmirror = [1,0,3,2]
validcommand = "$0123456789ipP-el?!os"
io = 'pPi'

class NetParser:
    def __init__(self,board):
        self.graph = nx.DiGraph()
        self.queue = deque()
        self.done = set()
        self.load_board(board)
        self.counter = 100
        
    def load_board(self,board):
        board = [x.replace('\n','') for x in board]
        mlen = max(len(x) for x in board)
        self.board = [x+' '*(mlen-len(x)) for x in board]
        self.w = mlen
        self.h = len(self.board)
        
    def add_man(self,p):
        self.queue.append(p)
        
    def get_moved(self,p):
        x,y,dir = p

        return Package(
                        (x+DIRS[dir][0]+self.w)%self.w,
                        (y+DIRS[dir][1]+self.h)%self.h,
                        dir)
    def get_next(self,p):
        
        
        p = self.get_moved(p)
        x,y,dir = p
        command = self.board[y][x]
            
        if command in "<^>v":
            dr = '<^>v'.index(command)
            yield Package(x,y,dr)
        elif command == '/':
            yield Package(x,y,formirror[dir])
        elif command == '\\':
            yield Package(x,y,backmirror[dir])
        elif command == '+':
            yield Package(x,y,(dir+1)%4)
            yield Package(x,y,(dir+3)%4)
        elif command == '#':
            yield Package(x,y,dir)
            yield Package(x,y,(dir+1)%4)
            yield Package(x,y,(dir+3)%4)
        elif command == 'S':
            yield Package(x,y,(dir+1)%4)
        elif command == 'Z':
            yield Package(x,y,(dir+3)%4)
            
        elif command == '*':
            pass
        else:
            yield Package(x,y,dir)
            
    def gen_fake_node(self,p):
        self.counter += 1
        return Package(p.x,p.y,self.counter)
            
    def add_start(self):
        for y,row in enumerate(self.board):
            for x,c in enumerate(row):
                if c == '&':
                    for dir in range(4):
                        self.queue.append(Package(x,y,dir))
    def process(self):
        self.add_start()
        while len(self.queue):
            p = self.queue.popleft()
            if p in self.done:
                print(f'node {p} already exists')
                continue
            self.done.add(p)
            for x in self.get_next(p):
                p2 = x#self.get_moved(x)
                self.add_man(p2)
                self.graph.add_edge(p,p2)
                
            print(f'node {p} added')
        for p in self.graph.nodes:
            c =  self.board[p.y][p.x]
            if c in validcommand:
                self.graph.nodes[p]['command'] = c
            else:
                self.graph.nodes[p]['command'] = ''
        for e in self.graph.edges:
            v = self.graph.edges[e]
            v['operations'] = self.graph.nodes[e[1]]['command']
            v['time'] = 1
        self.prune_branches()
        self.branch_prints()
        self.prune_branches()
        self.chain_ops()
        self.prune_empty_nodes()
        self.prune_branches()
        for e in self.graph.edges:
            v = self.graph.edges[e]
            v['fontname'] = 'Courier'
        for i,p in enumerate(self.graph.nodes):
            self.graph.nodes[p]['index'] = i
            if not list(self.graph.predecessors(p)):
                self.graph.nodes[p]['isstart'] = True
                self.graph.nodes[p]['color'] = 'red'
            else:
                self.graph.nodes[p]['isstart'] = False
            
    def prune_branches(self):
        while True:
            for p in self.graph.nodes:
                flag = True
                for b in list(self.graph.predecessors(p)):
                    if any(x in self.graph[b][p]['operations'] for x in io):
                        flag = False
                if not flag:
                    continue
                if not list(self.graph.successors(p)):
                    self.graph.remove_node(p)
                    print(f'prune unreachable {p}')
                    break
            else:
                break
    def branch_prints(self):
        for e in list(self.graph.edges):
            v = self.graph.edges[e]
            if v['operations'] in 'Pp':
                if v['time'] == 1 and not list(self.graph.successors(e[1])):
                    continue
                else:
                    fakenode = Package(e[0].x,e[0].y,e[0].dir+4)

                    self.graph.add_edge(e[0],fakenode)
                    v2 = self.graph[e[0]][fakenode]
                    v2['operations'] = v['operations']
                    v['operations'] = ''
                    v2['time'] = 1
                    
    def prune_empty_nodes(self):
         while True:
                
            for p in self.graph.nodes:
                ss = list(self.graph.successors(p))
                bb = list(self.graph.predecessors(p))
                if len(bb) == 1 and len(ss) == 1:
                    b = bb[0]
                    s = ss[0]
                    e1 = self.graph[b][p]
                    e2 = self.graph[p][s]
                    
                    if not e1['time'] and not s in self.graph.successors(b):
                        print(f'skip node {p}')
                        self.graph.add_edge(b,s)
                        e = self.graph[b][s]
                        e['operations'] = e2['operations']
                        e['time'] = e2['time']
                        self.graph.remove_node(p)
                        break
            else:
                break
                        
                        
            
    def chain_ops(self):
        while True:
            
            for p in self.graph.nodes:
                #connect chains if one starting and one ending
                ss = list(self.graph.successors(p))
                bb = list(self.graph.predecessors(p))
                #break chains upward
                if len(bb) == 1 and len(ss):
                    
                    b = bb[0]
                    e1 = self.graph[b][p]
                    if any(x in e1['operations'] for x in io):
                        continue
                        
                    else:
                        success = False
                        for s in ss:
                            
                            e2 = self.graph[p][s]
                            #if s in self.graph.successors(b):
                            #    continue
                            if any(x in e2['operations'] for x in io):
                                continue
                            if not e1['time']:
                                continue
                            f = self.gen_fake_node(p)
                            self.graph.add_edge(b,f)
                            self.graph.add_edge(f,s)
                            ne1 = self.graph[b][f]
                            ne2 = self.graph[f][s]
                            ne2['time'] = e1['time'] + e2['time']
                            ne1['time'] = 0
                            ne1['color'] = 'gray'
                            ne2['operations'] = e1['operations']+ e2['operations']
                            ne1['operations'] = ''
                            self.graph.remove_edge(p,s)
                            success = True
                        #self.graph.remove_node(p)
                        
                        if success:
                            print(f'broke {p}')
                            self.prune_empty_nodes()
                            break
                        
                        
                    
            else:
                break
            #break chains upward
            
            

            

    def display(self,name):
        for p in self.graph.nodes:
            v = self.graph.nodes[p]
            v['shape'] = 'point'
        for e in self.graph.edges:
            v = self.graph.edges[e]
            v['label'] = '{}.{}'.format(v['operations'],v['time'])
        dot = to_pydot(self.graph)
        dot.write_png(f'{name}.png')
        
    def compile(self,name):
        c = cppFile()
        c.load_nx(self.graph)
        c.export(name)
        Popen(('astyle',f'{name}.cpp'))
        Popen(('g++',f'{name}.cpp',f'-o{name}.out','-O3'))
                

                
class cppFile:
    struct = '''
    #include <iostream>
    #include <queue>
    #include <math.h>
    struct P
    {{
        int delay;
        int node_parent;
        double value;

        P(int delay, int node_parent, double value) : delay(delay), node_parent(node_parent), value(value)
        {{
        }}

        bool operator<(const struct P& other) const
        {{
            return delay > other.delay;
        }}
        
        void display(char* txt) 
        {{
            //std::cout << txt << " " << delay << ' ' << node_parent << ' ' << value << std::endl;
        }}
    }};
    
    int main()
    {{
        std::priority_queue<P> pq;
        {}
        while (!pq.empty()) {{
            P topel = pq.top();
            pq.pop();
            if (!pq.empty()) {{
                P nextel = pq.top();
                // if the two top packages can be combined in a single package, do that
                if (topel.delay==nextel.delay && topel.node_parent == nextel.node_parent)
                {{
                    // also remove second package
                    pq.pop();
                    // combine
                    //topel.display("combine1");
                    //nextel.display("combine2");
                    P VL = P(topel.delay,topel.node_parent,topel.value + nextel.value);
                    //VL.display("combined");
                    // add again
                    pq.push(VL);
                    //reset so multiple packages can be combined in a loop
                    continue;
                }}
            }}
            //topel.display("exec");
            {}
        }}
    }}
    '''
    def __init__(self):
        self.commands = []
        self.init = []
        
    def export(self,name='cpl'):
        with open(f'{name}.cpp','w') as file:
            file.write(self.struct.format('\n'.join(self.init),'\n'.join(self.commands))) 
        
    def add(self,v):
        self.commands.append(v)
        
    def add2(self,v):
        self.init.append(v)
        
    def close(self):
        self.add('}')
        
    def add_func_header(self,index):
        self.add(f'''
                 if (topel.node_parent=={index}) {{
                     int delay = topel.delay;
                     double value = topel.value;
                     ''')
        
    def add_func(self,startnode,endnode,delay,opts):
        self.add_func_header(startnode)
        self.add(f'delay += {delay};')
        depth = 1
        for x in opts:
            if x in '01234568789':
                self.add('value = value * 10 + '+x+';')
            elif x == '$':
                self.add('value = 0;')
            elif x == '-':
                self.add('value = -value;')
            elif x == 'e':
                self.add('value = exp(value);')
            elif x == 'l':
                self.add('value = log(value);')
            elif x == 'o':
                self.add('while (value > 0) {{delay += 1;value -= 1;}};')
            elif x == 'i':
                self.add('std::cin >> value;')
            elif x == 'p':
                self.add('std::cout << "out: " << value << std::endl;')
            elif x == 'P':
                self.add('std::cout << (char)value;')
            elif x == '?':
                self.add(f'if (value > 0) {{')
                depth += 1
            elif x == '!':
                self.add(f'if (value >= 0) {{')
                depth += 1
        self.add(f'pq.push(P(delay,{endnode},value));')
        for _ in range(depth):
            self.close()
            
    def load_nx(self,graph):
        for p in graph.nodes:
            if graph.nodes[p]['isstart']:
                endnode = graph.nodes[p]['index']
                self.add2(f'pq.push(P(0,{endnode},0.0));')
        for e in graph.edges:
            v = graph.edges[e]
            st = graph.nodes[e[0]]
            en = graph.nodes[e[1]]
            self.add_func(st['index'],en['index'],v['time'],v['operations'])
        

def build():
    nb = sys.argv[1]
    with open(f'{nb}.cbe') as f:        
        np = NetParser(f)
    np.process()
    np.display(nb)
    np.compile(nb)
    
if __name__ == "__main__":
    build()