# Conveyor_belt_esolang

Conveyor belt is a 2d esotheric programming language, inspired by Fission (https://esolangs.org/wiki/Fission) and befunge.
The program is fully composed of packages moving around on a conveyor belt in a factory. 

## Factory layout
Conveyor belt is a 2d language, where the source code is an abstract illustration of the layout of a factory.
Packages can move around inside this layout, but the layout cannot be modified in every way.
Each package has a position, a direction and a value. Packages move all at the same time, at the same speed. 
If packages move of the playing grid, they wrap around. 
If two packages have the same position and direction, they are merged together to a single package with as value
the sum of the values of the separate packagas.
There is no stack or storage, all data contained in the program is saved inside of the values of the packages.

### Program execution
Each cycle of the program, the following 3 actions are taken.
1. Each package performs the action defined by its position inside the source code.
2. Packages that have the same position and direction are merged
3. Packages move one unit dependant on their direction

### Components

Next follows a list of all the different actions that a package can take, by symbol

| Symbols        | Action           | 
| ------------- |-------------| 
| \<v\>^ | BELT: change direction of package|
|/\\ |MIRROR: reflects the directions based on the incoming direction|
|&| ASSEMBLER: creates 4 packages going in 4 directions at the start of the program |
|*| INCINERATOR: destroys packages that touch it|
|$| NULLIFY: sets the value of the package to zero|
|0123..9| DEFINE: multiplies the current value by ten and adds the given number|
|p|PRINT: print the current value as number|
|P|PRINT: print the current value module 256 as character (rounded to nearest int)|
|i|INPUT: get user input|
|+|SPLITTER: copies package into two packages, on making a turn left and one making a turn right |
|SZ| ROTATE: turn a package left or right|
|#|SPLITTER: copies package into three packages, on making a turn left, one making a turn right and the last one going straight on |
|-|NEGATE: multiplies the package with -1|
|l|LOGARITM: takes the base e logaritm of the value|
|e|EXPONENT: performs e^x at the value|
|?|destroys the package if the value is smaller than 0|
|!|destroys the package if the value is smaller or equal than 0|
|o|DELAY: while a package is one here, it cannot move if the value is larger than zero. Each tick its value deceases by one.|
|s|STICKY: while a package is one here, it cannot move if the value is smaller or equal than zero. (not supported yet by compiler)|


Note that for print and input statements, the interpreter and compiler are allowed to execute them in any order if multiple packages hit the statements the same tick.

## Components

The above components can be combined in the factory layout to create more useful systems. Here one finds some examples:

### Single direction assembler
```
 *
*&
 *
```
Ejects a package in a single direction

### Set to constant value
```
>>>$567>>>
```
Sets the value of a package to a contant integer

### Set to infinity
```
>>>$l->>>
```
Sets the value of a package to infinity

### Check if zero

```
>>>-!-!>>>
```
Only lets packages trough that are zero

### Check if nonzero

```
  >-?v
>>+  >>>>
  >>?^
```
Only lets packages trough that are not zero

### Summation
```
>>>v
   >>>>>
>>>^
```
Sums two packages (if they arrive at the same time.)

### Rectifier

```
   >-?v
>>>+  >>>>
   >>>^
```
Sets the value of a package to 0 if it is negative

### Absolute value

```
   >-?v
>>>+  >>>>
   >>!^
```

### Substration
```
>>-v
   >>>>>
>>>^
```

### Multiplication (simple)
```
>>lv
   >>e>>
>>l^
```
Multiplies two packages with positive values (if they arrive at the same time.)


### Multiplication (full)
```
 TODO: base concept> split each input into 4 belts,
 negate 2 of 4 belts. rectify erevything, multiply each pair, 
 sum 4 results.
```
Multiplies two packages (if they arrive at the same time.)

### Division (simple)

```
>>l-v
    >>e>>
>>l>^
```
Divides two packages with positive values (if they arrive at the same time.)

### Gate
```
>>>$1->->v
 >>>>e# ^>lv
      > v  >e?l>>>>
        >>l^   
```
Allows the bottom package to flow trough the system unchanged if a top package hits the system at the same time, and stops it otherwise.

### Loop

Launches a number of packages equal to the number of original packages.

```  
    >$1-v
   >#*  >v
>> ^>   ^?
 +       <
 >$>>>>>>>>
```



## Examples

### Hello world

```
 **
*&#$72 P $119P 
 *#$101P$114  P
 *#$108PP     P
 *#$111 P  P
 *#$32  P$100P
  *
```

### First N fibonacy numbers

```
 *            *
*&* >$1-v     p
 i >#*  >       v
 > ^>   ^       ?
 +              <
 > $1->->v     
  >   # ^>lv   
      > v  >e?+    *
        >>l^       p
    v < >v   v<    1
  ^  <+      <1o51$&*
   v<^<     <      *
    
     
   >    ^>  ^
              
              
```



